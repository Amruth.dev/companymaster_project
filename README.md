# Analytics on registration in Maharashtra.

##### To convert raw open data into plots, that tell a story on the state of company registration in Maharashtra.

## Python Version
python 3.+ 

## Module Installation

Use the package manager pip to install .

```bash
pip install matplotlib
```

## Target Achieved

#### 1: Histogram of Authorized Cap- Bar Plot

Plot a histogram on the "Authorized Capital" (column: AUTHORIZED_CAP) with the following intervals

- <= 1L
- 1L to 10L
- 10L to 1Cr
- 1Cr to 10Cr
- > 10Cr



#### 2: Bar Plot of company registration by year

From the column, DATE_OF_REGISTRATION parse out the registration year. Using this data, plot a bar plot of the number of company registrations, vs. year.

#### 3: Company registration in the year 2015 by the district.
plot bar chart of Company registration in the year 2015 by the district


#### 4: Grouped Bar Chart 

Plot a Grouped Bar Plot by aggregating registration counts over:

- Year of registration
- Principal Business Activity
- Plot only top 5 Prinicipal Business Activity for last 10 years


## Outputs
[https://drive.google.com/drive/u/1/folders/1hs7O0KnjRUA0DgXBxqSh_KFbMsZxhjiW](https://drive.google.com/drive/u/1/folders/1hs7O0KnjRUA0DgXBxqSh_KFbMsZxhjiW)
## Run Instructions
- Each problem statement has seperate .py files.

- Make sure that csv files are in the same directory.

- Run the code in your desired IDEs