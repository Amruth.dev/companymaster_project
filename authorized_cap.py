import csv
import matplotlib.pyplot as plt

def authorized_cap(company_detail):
    authorized_cap_intervals = {
        '<= 1L': 100000,
        '1L to 10L': 1000000,
        '10L to 1Cr': 10000000,
        '1Cr to 10Cr': 100000000,
        '> 10Cr': float('inf')
    }
    # count=1
    auth_cap_count={interval:0 for interval in authorized_cap_intervals}
    # print(auth_cap_count)
    for company in company_detail:
        # if(count<=1):
        #     print(company)
        #     count+=1
        auth_cap=float(company["AUTHORIZED_CAP"])
        for interval,cap_limit in authorized_cap_intervals.items():
            if(auth_cap<=cap_limit):
                auth_cap_count[interval]+=1
                break
    return auth_cap_count
   
            


def plot_histogram_authorize_cap(auth_cap_count):
    """plot_histogram_authorize_cap"""
    intervals = list(auth_cap_count.keys())
    counts = list(auth_cap_count.values())
    plt.bar(intervals,counts)
    plt.title("histogram of authorized caps")
    plt.xlabel("intervals")
    plt.ylabel("num of companies")
    plt.xticks(rotation=45)
    plt.show()




def execute():
    #changed encoding from utf-8 to encoding='ISO-8859-1'
    with open('./archive/Maharashtra.csv','r',encoding='ISO-8859-1') as file:
        company_detail=csv.DictReader(file)
        result=authorized_cap(company_detail)
        plot_histogram_authorize_cap(result)

execute()