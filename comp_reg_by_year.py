import csv
import matplotlib.pyplot as plt


def extract_registration_year(date_in_str):
    parts=date_in_str.split("-")
    # print(parts)
    year=parts[-1]
    # print(year)
    if(len(year)==2):
        year="19"+year
    return year

def company_reg_by_year(company_details):
    """company_reg_by_year"""
    company_reg={}
    for company in company_details:
        registration_year=extract_registration_year(company["DATE_OF_REGISTRATION"])
        # company_name=company["Company_Name"]
        company_reg[registration_year]=company_reg.get(registration_year,0)+1
    return company_reg
        
def plot_company_reg_by_year(reg_years):
    reg_year=list(reg_years.keys())
    num_of_companies=list(reg_years.values())

    plt.bar(reg_year,num_of_companies)
    plt.title("company registration by year")
    plt.xticks(rotation=90)
    plt.xlabel("years")
    plt.ylabel("number of companies")
    plt.show()

def execute():
    """execute function"""
    with open("./archive/Maharashtra.csv","r",encoding='ISO-8859-1') as file:
        company_details=csv.DictReader(file)
        reg_years=company_reg_by_year(company_details)
        plot_company_reg_by_year(reg_years)
execute()
