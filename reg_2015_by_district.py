
import csv
import matplotlib.pyplot as plt
import os.path

def extract_registration_year(date_in_str):
    parts=date_in_str.split("-")
    year=parts[-1]
    if(len(year)==2):
        year="20"+year
    return year


def extract_zip_code(address_in_str):
    parts=address_in_str.split(",")
    splits=parts[-1]
    zip_code=splits[-6:]
    return zip_code
        

def make_csv_zip_vs_district(data,file_path):
    if not os.path.isfile(file_path):
        with open(file_path,'w',newline="") as file:
            writer=csv.writer(file)
            writer.writerows(data)
        print("csv generated successfully")
    else:
        print("file already exists")


def company_reg2015_by_district(company_details):
    """company_reg_by_year"""
    # company_reg={}
    zip_code=set()
    for company in company_details:
        registration_year=extract_registration_year(company["DATE_OF_REGISTRATION"])
        
        if(registration_year=='2015'):
            zip=extract_zip_code(company["Registered_Office_Address"])
            if(zip):
                zip_code.add(zip)
    return zip_code


def calculate_num_reg_vs_district(pincode_district,zipcodes):
    num_reg_vs_district = {}
    for district in pincode_district:
        if district["Pin Code"] in zipcodes:
            # print(district["Pin Code"])
            num_reg_vs_district[district["District"]]=num_reg_vs_district.get(district["District"],0)+1
    # print(num_reg_vs_district)
    return num_reg_vs_district



def plot_company_reg_by_year(reg_years):
    district = list(reg_years.keys())
    num_of_registration = list(reg_years.values())

    plt.bar(district,num_of_registration)
    plt.title("number_of_registration ve district")
    plt.xticks(rotation=90)
    plt.xlabel("district")
    plt.ylabel("number of registration")
    plt.show()

data = [
    ["Pin Code", "District"],
    ["414001", "Ahmednagar"],
    ["444001", "Akola"],
    ["444601", "Amravati"],
    ["431001", "Aurangabad"],
    ["401101", "Thane"],
    ["421308", "Thane"],
    ["442401", "Chandrapur"],
    ["424001", "Dhule"],
    ["421201", "Thane"],
    ["416115", "Kolhapur"],
    ["425001", "Jalgaon"],
    ["421301", "Thane"],
    ["416003", "Kolhapur"],
    ["413512", "Latur"],
    ["423203", "Nashik"],
    ["401107", "Thane"],
    ["400001", "Mumbai"],
    ["440001", "Nagpur"],
    ["431601", "Nanded"],
    ["422001", "Nashik"],
    ["431401", "Parbhani"],
    ["412303", "Pune"],
    ["411001", "Pune"],
    ["416416", "Sangli"],
    ["413001", "Solapur"],
    ["400601", "Thane"],
    ["1421002", "Thane"],
    ["401208", "Palghar"],
    ["401303", "Palghar"]
]

def execute():
    """execute function"""
    file_path='./archive/pincode_district.csv'
    make_csv_zip_vs_district(data,file_path)


    with open("./archive/Maharashtra.csv","r",encoding='ISO-8859-1') as file:
        company_details=csv.DictReader(file)
        reg_by_district=company_reg2015_by_district(company_details)

    with open("./archive/pincode_district.csv","r",encoding='ISO-8859-1') as file1:
        pincode_district=csv.DictReader(file1)
        calculate=calculate_num_reg_vs_district(pincode_district,reg_by_district) 
        plot_company_reg_by_year(calculate)
execute()
