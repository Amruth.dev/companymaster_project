from csv import DictReader
import matplotlib.pyplot as plot_graph


def calculateYear(date):
    year = date[6:8]
    if len(date) > 8:
        return int(date[:4])
    elif int(year) >= 22:
        return int('19'+year)
    return int('20'+year)


def count_reg(file_path):
    with open(file_path, 'r', encoding='latin-1') as maharashtra_files:
        companies_reader = DictReader(maharashtra_files)
        companies_dict = {}
        
        for company in companies_reader:
            date_of_registration = company.get('DATE_OF_REGISTRATION')
            business_activity = company.get('PRINCIPAL_BUSINESS_ACTIVITY_AS_PER_CIN')
            
            if date_of_registration != 'NA':
                year = calculateYear(date_of_registration)
                
                if business_activity not in companies_dict:
                    companies_dict[business_activity] = {}
                
                if year not in companies_dict[business_activity]:
                    companies_dict[business_activity][year] = 0
                
                companies_dict[business_activity][year] += 1
        
        all_years = [year for years in companies_dict.values() for year in years]
        unique_years = sorted(set(all_years))
        years = unique_years[-5:]
        
        top_five_business_company_data = calculate_last_five_year_top_business_activity(companies_dict, years)
        
        return {tuple(years): top_five_business_company_data}



def calculate_last_five_year_top_business_activity(companies_dict, years):
    top_business_activity_company = []
    
    for year in years:
        all_companies = {company: companies_dict[company][year] for company in companies_dict if year in companies_dict[company]}
        
        sorted_all_company_year_wise = sorted(all_companies.items(), key=lambda item: item[1])
        top_five_company = sorted_all_company_year_wise[-5:]
        top_business_activity_company.append(top_five_company)
    
    return top_business_activity_company


def plot_last_five_year_top_business_activity(companies_data):
    year_data = next(iter(companies_data))
    top_five_company_year_wise_data = companies_data[year_data]
    
    width = 0.3
    position = [0, 2, 4, 6, 8]
    height = 100
    
    for index in range(len(top_five_company_year_wise_data)):
        five_business_company = [company[index][0] for company in top_five_company_year_wise_data]
        paid_up_capital = [company[index][1] for company in top_five_company_year_wise_data]
        
        if index != 0:
            position = [pos + width for pos in position]
        
        plot_graph.bar(position, paid_up_capital, width)
        
        for pos, company_name in zip(position, five_business_company):
            plot_graph.text(pos, height, company_name, ha='center', rotation=90)
    
    plot_graph.title('Top Five Business Activity Company Year Wise')
    plot_graph.xlabel('Year')
    plot_graph.ylabel('Paid Up Capital')
    
    position_set = [pos + 2.5 * (width / 2) for pos in [0, 2, 4, 6, 8]]
    plot_graph.xticks(position_set, year_data)
    plot_graph.legend(bbox_to_anchor=(1, 1), loc='upper left')
    plot_graph.show()


def execute():
    file_path = './archive/Maharashtra.csv'
    companies_data = count_reg(file_path)
    plot_last_five_year_top_business_activity(companies_data)


if __name__ == '__main__':
    execute()
